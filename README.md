# Active Directory Setup

* Steps:
    1. [Setup VMs](https://gitlab.com/cr4ckb0x/Active_Directory/-/tree/main/00-Setup_VMs)
    2. [Install Domain Controller](https://gitlab.com/cr4ckb0x/Active_Directory/-/tree/main/01-Install_DC)
    3. [Setup Client Systems](https://gitlab.com/cr4ckb0x/Active_Directory/-/tree/main/02-Setup_Client_Systems)
    4. [Automating Domain Users and Groups](https://gitlab.com/cr4ckb0x/Active_Directory/-/tree/main/03-Automating_Domain_Users)