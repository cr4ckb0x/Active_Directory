param( [Parameter(Mandatory=$true)] $OutputJSONFile)

#---------- INPUT FILES ----------#
# $group_names = [System.Collections.ArrayList](Get-Content ".\03-Automating_Domain_Users\data\group-names.txt")
$groups_names = (Import-Csv -Path .\03-Automating_Domain_Users\data\group_names.csv -Delimiter ",")
$first_names = [System.Collections.ArrayList](Get-Content ".\03-Automating_Domain_Users\data\first-names.txt")
$last_names =  [System.Collections.ArrayList](Get-Content ".\03-Automating_Domain_Users\data\last-names.txt")
$passwords = [System.Collections.ArrayList](Get-Content ".\03-Automating_Domain_Users\data\password.txt")
#---------- INPUT FILES ----------#
# $num_groups = 13
$groups = @()
$num_users = 100
$users = @()

# for ($i=0;$i -lt $num_groups; $i++){
#     $new_group = $(Get-Random -InputObject $group_names)
#     $groups += @{"name"=$new_group}
#     $group_names.Remove($new_group)
# }
foreach ($grp in $groups_names){
    $display_name = $grp.Display_Name
    $name = $grp.Name
    $desc = $grp.Description
    $groups += @{
        Display_Name=$display_name
        Name=$name
        Description=$desc
    }

}
for ($i=0;$i -lt $num_users; $i++){
    # $culture = (Get-Culture).TextInfo
    $firstName = $(Get-Random -InputObject $first_names)
    $lastName = (Get-Culture).TextInfo.ToTitleCase(($(Get-Random -InputObject $last_names)).ToLower())
    $pswd = $(Get-Random -InputObject $passwords)
    $users +=  @{
        "firstname"="$firstName"
        "lastname"="$lastName"
        "password"="$pswd"
        "group"=(Get-Random -InputObject $groups.Name)
    }

    $first_names.Remove($firstName)
    $last_names.Remove($lastName)
    $passwords.Remove($pswd)
}
# echo $groups

@{
    "domain"="xyz.com"
    "groups"=$groups
    "users"=$users
} | ConvertTo-Json | Out-File $OutputJSONFile