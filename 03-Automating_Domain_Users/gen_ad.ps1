param(
    [Parameter(Mandatory=$true,Position=1)][string]$JSONFile,
    [switch]$Delete
)

function WeakenSecurityPolicies(){
    secedit /export /cfg C:\Windows\Tasks\secpol.cfg
    (Get-Content C:\Windows\Tasks\secpol.cfg).Replace("PasswordComplexity = 1","PasswordComplexity = 0").Replace("MinimumPasswordLength = 7","MinimumPasswordLength = 1") | Out-File C:\Windows\Tasks\secpol.cfg
    secedit /configure /db C:\Windows\security\local.sdb /cfg C:\Windows\Tasks\secpol.cfg /areas SECURITYPOLICY
    Remove-Item -Force C:\Windows\Tasks\secpol.cfg -Confirm:$false
}

function StrengthenSecurityPolicies(){
    secedit /export /cfg C:\Windows\Tasks\secpol.cfg
    (Get-Content C:\Windows\Tasks\secpol.cfg).Replace("PasswordComplexity = 0","PasswordComplexity = 1").Replace("MinimumPasswordLength = 1","MinimumPasswordLength = 7") | Out-File C:\Windows\Tasks\secpol.cfg
    secedit /configure /db C:\Windows\security\local.sdb /cfg C:\Windows\Tasks\secpol.cfg /areas SECURITYPOLICY
    Remove-Item -Force C:\Windows\Tasks\secpol.cfg -Confirm:$false
}

function RemoveADGroups(){
    param( [Parameter(Mandatory=$true)] $groupObject )
    $name = $groupObject.name
    # Command to add AD Groups
    Remove-ADGroup -Identity $name -Confirm:$false
}

function CreateADGroups(){
    param( [Parameter(Mandatory=$true)] $groupObject )
    $name = $groupObject.name
    $title = $groupObject.title
    $desc = $groupObject.description
    # Command to add AD Groups
    New-ADGroup -Name $name -DisplayName $title -Description $desc -GroupScope Global
}

function RemoveADUser() {
    param( [Parameter(Mandatory=$true)] $userObject )
    $firstName = $userObject.firstname
    $lastName = $userObject.lastname
    $samAccountName = ($firstName[0]+$lastName).ToLower()
    Remove-ADUser -Identity $samAccountName -Confirm:$false
    try {
        Get-ADGroup -Identity "$group"
        Add-ADGroupMember -Identity $group -Members $username
    }
    catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
    {
        Write-Warning "Active Directory Group ($group) NOT found!"
    }
}

function CreateADUser() {
    param( [Parameter(Mandatory=$true)] $userObject )
    $firstName = $userObject.firstname
    $lastName = $userObject.lastname
    $password = $userObject.password
    $group = $userObject.group
    # $password = $firstName.ToLower()+"."+$lastName.ToLower()+"@12345"
    # Generate "First Initial, Last Name" structure
    $username = ($firstName[0]+$lastName).ToLower()
    $samAccountName = $username
    $principalName = $samAccountName
    # Command to Add AD User
    # Uncomment the line below to run the command
    New-ADUser -Name "$firstName $lastName" -GivenName $firstName -Surname $lastName -SamAccountName $samAccountName -UserPrincipalName $principalName@Global:Domain -AccountPassword (ConvertTo-SecureString $password -AsPlainText -Force) -PassThru | Enable-ADAccount
    try {
        Get-ADGroup -Identity "$group"
        Add-ADGroupMember -Identity $group -Members $username
    }
    catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
    {
        Write-Warning "Active Directory Group ($group) NOT found!"
    }
}

$json = ( Get-Content $JSONFile | ConvertFrom-Json)
$Global:Domain = $json.domain

if ($Delete.IsPresent){
    # Adding AD Users
    foreach( $user in $json.users ){
        RemoveADUser $user
    } 

    # Adding AD Groups
    foreach( $group in $json.groups){
        RemoveADGroups $group
    }
    # Re-strengthen Security Policies
    StrengthenSecurityPolicies
} else {
    # Weaken Security Policies
    WeakenSecurityPolicies
    
    # Adding AD Groups
    foreach( $group in $json.groups){
        CreateADGroups $group
    }

    # Adding AD Users
    foreach( $user in $json.users ){
        CreateADUser $user
    }   
}