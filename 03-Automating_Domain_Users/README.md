# 03 Automating Domain Users


* Use one of the two options:<br>
1) Create a vulnerable active directory that's allowing you to test most of active directory attacks in local lab -> [VulnAD](https://github.com/WazeHell/vulnerable-AD)<br>
**OR**
2) Follow the steps below:<br>
    (a) Run `random_domain.ps1` script to generate random users and groups.<br>
    (b) Run `gen_ad.ps1` script to add the generated list of users and groups in the Active Directory

**Important**: In case of password policy error, use the command below in powershell in administrative mode in order to remove the secure password policy.
```powershell
> secedit /export /cfg C:\Windows\Tasks\secpol.cfg
> (Get-Content C:\Windows\Tasks\secpol.cfg).replace("PasswordComplexity = 1","PasswordComplexity = 0") | Out-File C:\Windows\Tasks\secpol.cfg
> secedit /configure /db C:\Windows\security\local.sdb /cfg C:\Windows\Tasks\secpol.cfg /area SECURITYPOLICY
> rm -force C:\Windows\Tasks\secpol.cfg -confirm:$false
```