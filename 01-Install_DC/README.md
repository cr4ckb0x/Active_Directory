#  01 Installing Domain Controller

***Important*: Ensure to take regular snapshots after each step!!**
1. Use `sconfig` to:
    - Change the hostname
    - Change te IP Address to static
    - Change the DNS Server to our own IP Address
2. Install the Active Directory Windows Feature
```shell
> Install-WindowsFeature AD-Domain-Services -IncludeManagementTools
> import-Module ADDSDeployment
> Install-ADDSForest
```
* Enter the *Domain Name* and *safe mode password* (twice for confirmation).
* Reboot the Domain Controller System
```shell
> Get-NetIPAddress -IPAddress <IP_Address_of_DC>
> Set-DNSClientServerAddress -InterfaceIndex <Interface_Index_number> -ServerAddresses <IP_Address_of_DC_Server>
>Get-DNSClientServerAddress -InterfaceIndex <Interface_Index_number>
```
**Shutdown the Domain Controller and take a snapshot**