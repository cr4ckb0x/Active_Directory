# 02 Setup Domain Client System

1. Change DNS of the local system, to ensure that the system recognizes the domain setup.
    '''shell
    > Set-DNSClientServerAddress -InterfaceIndex <Interface_Index_Number> -ServerAddresses <DC_Server_IP_Address>
    ```
2. Add the system to the domain
    ```shell
    > add-computer -DomainName <Domain_Name> -Credential <Domain_Name>\\<user_name> -Force -Restart
    ```
    *Important*: Ensure that you added the user previously to the domain